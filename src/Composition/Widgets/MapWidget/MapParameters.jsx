import React from 'react';

class MapParameters extends React.Component {
  constructor(props) {
    super(props);
    this.state = props.parameters;
  }

  ChangeParams = (e) => {
    if (e.target.type === 'checkbox') this.props.onParamChange(e.target.name, e.target.checked);
    else this.props.onParamChange(e.target.name, e.target.value);

    var params = this.state;
    params[e.target.name] = e.target.value;
    this.setState(params);
  };

  render() {
    return (
      <div id='mapWidgetProps'>
        <div className='group-label'>Параметры карты</div>
        <div className='form-group'>
          <div className='label'>Рабочее пространство:</div>
          <div className='value'>
            <select name='workspace' value={this.state.workspace} onChange={this.ChangeParams}>
              {JSON.parse(localStorage.getItem('AGIS_WORKSPACES')).map((w, index) => {
                return (
                  <option key={index} value={index}>
                    {w.display_name}
                  </option>
                );
              })}
            </select>
          </div>
        </div>
        <div className='form-group'>
          <div className='label'>Имя:</div>
          <div className='value'>
            <input value={this.props.parameters.name} name='name' onChange={this.ChangeParams} type='text'></input>
          </div>
        </div>
      </div>
    );
  }
}

export default MapParameters;
