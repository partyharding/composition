import React from 'react';
import mapboxgl from 'mapbox-gl';
import 'mapbox-gl/dist/mapbox-gl.css';
import DefaultWidgetParameters from '../DefaultWidget/DefaultWidgetParameters';
import WidgetWrapper from '../DefaultWidget/WidgetWrapper';
import MapParameters from './MapParameters';
import './MapWidget.css';
import canvasToImage from 'canvas-to-image';

class MapWidget extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      parameters: {
        name: 'Карта ' + props.index,
        workspace: Number(localStorage.getItem('AGIS_WORKSPACE_INDEX')),
      },
      defaultParameters: {
        hasBackGround: true,
        backGroundColor: '#ffffff',
        borderWidth: 1,
        borderColor: '#2191FB',
      },
      mapLoaded: false,
      imageUrl: '',
      scale: 0
    };
    this.legendRefs = [];
    this.spriteData = null;
    this.map = null;
    this.workspaces = JSON.parse(localStorage.getItem('AGIS_WORKSPACES'));
    this.requestHeaders = new Headers({ Authorization: localStorage.getItem('AGIS_TOKEN') });
    this.styleDataRecieved = false;
  }

  RenderParameters = () => {
    return (
      <div key={this.props.index}>
        <MapParameters parameters={this.state.parameters} onParamChange={this.ChangeParameter} />
        <DefaultWidgetParameters parameters={this.state.defaultParameters} onParamChange={this.ChangeDefaultParameter} />
      </div>
    );
  };

  ChangeParameter = (paramName, newVal) => {
    //clone parameters to avoid direct state mutate warning
    var params = this.state.parameters;
    params[paramName] = newVal;

    this.setState({
      parameters: params,
    });

    if (paramName === 'workspace') this.SwitchWorkspace(newVal);
  };

  ChangeDefaultParameter = (paramName, newVal) => {
    var params = this.state.defaultParameters;
    params[paramName] = newVal;
    this.setState({
      defaultParameters: params,
    });
  };

  mapInit = () => {
    this.map = new mapboxgl.Map({
      container: 'mapbox-container-' + this.props.index,
      style: this.workspaces[localStorage.getItem('AGIS_WORKSPACE_INDEX')].url,
      center: [67, 48],
      zoom: 2,
      preserveDrawingBuffer: true,
      transformRequest: (url, resourceType) => {
        if (resourceType === 'Style')
          return {
            url: url,
            headers: { Authorization: localStorage.getItem('AGIS_TOKEN') },
          };
      },
    });
    this.map.on('styledata', this.StyleData);
    this.map.on('load', this.MapLoaded);
    this.map.on('idle', this.MapIdle);
  };

  componentDidMount = () => {
    this.mapInit();
  };

  GetSpriteData = async (workspace) => {
    var spriteData = null;
    var stylesResponse = await fetch(this.workspaces[workspace].url, { headers: this.requestHeaders });
    var json = await stylesResponse.json();
    if (json.sprite) {
      var imageFetch = await fetch(json.sprite + '.png', { headers: this.requestHeaders });
      var imageBlob = await imageFetch.blob();
      var positionsFetch = await fetch(json.sprite + '.json', { headers: this.requestHeaders });
      var positionsJson = await positionsFetch.json();
      spriteData = { image: URL.createObjectURL(imageBlob), positions: positionsJson };
    }
    return spriteData;
  };

  SwitchWorkspace = (workspace) => {
    this.switchingWorkspace = true;
    this.styleDataRecieved = false;
    this.map.setStyle(this.workspaces[workspace].url);
  };

  MapLoaded = () => {
    this.setState({ mapLoaded: true });
  };

  StyleData = () => {
    if (!this.styleDataRecieved) {
      this.GetSpriteData(this.state.parameters.workspace).then((spriteData) => {
        this.spriteData = spriteData;
        if (this.legendRefs.length > 0) {
          //Update legend ref here
          this.UpdateAttachedLegends();
        }
      });
    }
    this.styleDataRecieved = true;
  };

  GetScaleByZoom = () => {
    var pixels_per_meter = 40075.016686 * 1000 / 512; // 6378137.0
    var resolution = pixels_per_meter  * Math.cos(this.map.getCenter().lat *  Math.PI / 180) / (Math.pow(2, this.map.getZoom()));
    var screen_dpi = 96 * window.devicePixelRatio;
    var scale = screen_dpi * (1/0.0254 * resolution);
    return Math.round(scale/100);
}

  MapIdle = () => {
    this.UpdateAttachedLegends();
    this.setState({scale: this.GetScaleByZoom()})
  };

  UpdateAttachedLegends = () => {
    this.legendRefs.map((legeldRef) => {
      if (legeldRef !== null) {
        legeldRef.ref.UpdateLegend();
      }
    });
  };

  AddLegendRef = (ref) => {
    this.legendRefs.push({ index: ref.props.index, ref: ref });
  };

  RemoveLegendRef = (index) => {
    var indexOfRemovingRef = this.legendRefs.findIndex((ref) => {
      return ref !== null && ref.index === index;
    });
    this.legendRefs.splice(indexOfRemovingRef, 1, null);
  };

  componentWillUnmount = () => {
    this.map.off('styledata', this.StyleData);
    this.map.off('load', this.MapLoaded);
  };

  onWidgetRemove = () => {
    this.props.onWidgetRemove(this.props.index);
    this.legendRefs.map((legeldRef) => {
      if (legeldRef !== null) legeldRef.ref.UnsetMapRef();
    });
  };

  render() {
    return (
      <WidgetWrapper
        width={300}
        height={300}
        parameters={this.props}
        defaultparameters={this.state.defaultParameters}
        onChooseWidget={() => {
          this.props.onChooseWidget(this.props.index);
        }}
        onWidgetRemove={this.onWidgetRemove}
        rndProps={{
          onResizeStop: () => {
            this.map.resize();
          },
          scale: this.props.paperScale,
        }}
        dragHandler={true}>
        <div className='map-widget'>
          <div className='mapbox-name'>{this.state.parameters.name}</div>
          <div className='message'>{this.state.mapLoaded ? '' : 'Загружается карта...'}</div>
          <div className='mapbox-container' id={'mapbox-container-' + this.props.index}></div>
          <div className='mapbox-scale'>1 : {this.state.scale}</div>
        </div>
      </WidgetWrapper>
    );
  }
}

export default MapWidget;
