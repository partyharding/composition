import React from 'react';
import MapWidget from './MapWidget';
import './../DefaultWidget/WidgetItem.css';
import textIcon from './../Icons/earth-fill.svg';

class MapItem extends React.Component {
    OnDragEnd = (e) => {
        this.props.onAddWidget(e, <MapWidget/>, "map");
    }

    render() {
        return <div className="widget-item" draggable onDragEnd={this.OnDragEnd}>
            <div className="widget-item-icon">
                <img alt="" src={textIcon}></img>
            </div> 
            <div className="widget-item-label">
                Карта
            </div>
        </div>;
    }
}

export default MapItem;