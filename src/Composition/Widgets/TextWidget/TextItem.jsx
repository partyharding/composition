import React from 'react';
import TextWidget from './TextWidget';
import './../DefaultWidget/WidgetItem.css';
import textIcon from './../Icons/text.svg';

class TextItem extends React.Component {
    OnDragEnd = (e) => {
        this.props.onAddWidget(e, <TextWidget/>, "text");
    }

    render() {
        return <div className="widget-item" draggable onDragEnd={this.OnDragEnd}>
            <div className="widget-item-icon">
                <img alt="" src={textIcon}></img>
            </div> 
            <div className="widget-item-label">
                Текст
            </div>
        </div>;
    }
}

export default TextItem;