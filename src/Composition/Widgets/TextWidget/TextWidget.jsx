import React from 'react';
import DefaultWidgetParameters from '../DefaultWidget/DefaultWidgetParameters';
import WidgetWrapper from '../DefaultWidget/WidgetWrapper';
import TextParameters from './TextParameters';

class TextWidget extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            "parameters":{
                "text":"Текст",
                "fontSize":32,
                "italic":false,
                "bold":false,
                "underline":false,
                "align":"left",
                "color":"#000000"
            },
            "defaultParameters":{
                "hasBackGround":true,
                "backGroundColor":"#ffffff",
                "borderWidth":1,
                "borderColor":"#2191FB"
            }
        }
    }

    RenderParameters = () => {
        return  <div key={this.props.index}>
            <TextParameters 
                parameters={this.state.parameters} 
                onParamChange={this.ChangeParameter}
            />
            <DefaultWidgetParameters
                parameters={this.state.defaultParameters} 
                onParamChange={this.ChangeDefaultParameter}
            />
        </div> 
    }

    ChangeParameter = (paramName, newVal) => {
        //clone parameters to avoid direct state mutate warning
        var params = this.state.parameters;
        params[paramName] = newVal;
        this.setState({
            "parameters":params
        });
    }

    ChangeDefaultParameter = (paramName, newVal) => {
        var params = this.state.defaultParameters;
        params[paramName] = newVal;
        this.setState({
            "defaultParameters":params
        });
    }

    ChoseWidget = () => {
        this.props.onChooseWidget(this.props.index);
    }

    WidgetRemove = () => {
        this.props.onWidgetRemove(this.props.index);
    }

    render() {
        return (
            <WidgetWrapper
                width = {200}
                height = {50}
                parameters = {this.props}
                defaultparameters = {this.state.defaultParameters}
                onChooseWidget = {this.ChoseWidget}
                onWidgetRemove = {this.WidgetRemove}
                rndProps={{
                    scale:this.props.paperScale
                }}
            >
                <div
                     style={
                        {
                            "fontSize":parseInt(this.state.parameters.fontSize),
                            "fontStyle":this.state.parameters.italic && "italic",
                            "fontWeight":this.state.parameters.bold && "bold",
                            "textDecoration":this.state.parameters.underline && "underline",
                            "textAlign":this.state.parameters.align,
                            "color":this.state.parameters.color
                        }
                    }
                >
                    {this.state.parameters.text}
                </div>
            </WidgetWrapper>    
        )
    }
}

export default TextWidget;