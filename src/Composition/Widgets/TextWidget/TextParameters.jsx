import React from 'react';
import './TextParameters.css';

class TextParameters extends React.Component {
    constructor(props){
        super(props);
        this.state = props.parameters;
    }

    ChangeParams = (e) => {
        if(e.target.type === "checkbox")
            this.props.onParamChange(e.target.name, e.target.checked);
        else 
            this.props.onParamChange(e.target.name, e.target.value);
        
        var params = this.state;
        params[e.target.name] = e.target.value;
        this.setState(params);
    }
    
    render() {
        return <div id="textWidgetProps">
            <div className="group-label">Параметры текста</div>
            <div className="form-group">
                <div className="label">Текст:</div>
                <div className="value">
                    <input value={this.props.parameters.text} name="text" onChange={this.ChangeParams} type="text"></input>
                </div>
            </div>
            <div className="form-group">
                <div className="label">Размер шрифта:</div>
                <div className="value">
                    <input value={this.props.parameters.fontSize} name="fontSize" onChange={this.ChangeParams} type="number"></input>
                </div>
            </div>
            <div className="form-group">
                <div className="label">Стиль шрифта:</div>
                <div className="value">
                    <label htmlFor="param-bold">Жирный</label>
                    <input id="param-bold" name="bold" onChange={this.ChangeParams} checked={this.props.parameters.bold} type="checkbox"></input>
                </div>
                <div className="value">
                    <label htmlFor="param-italic">Наклон</label>
                    <input id="param-italic" name="italic" onChange={this.ChangeParams} checked={this.props.parameters.italic} type="checkbox"></input>
                </div>
                <div className="value">
                    <label htmlFor="param-underline">Подчёркивание</label>
                    <input id="param-underline" name="underline" onChange={this.ChangeParams} checked={this.props.parameters.underline} type="checkbox"></input>
                </div>
            </div>
            <div className="form-group">
                <div className="label">Равнение текста:</div>
                <div className="value">
                    <input id="param-align-left" onChange={this.ChangeParams} type="radio" name="align" value="left"></input>
                    <label htmlFor="param-align-left">Слева</label>
                </div>
                <div className="value">
                    <input id="param-align-center" onChange={this.ChangeParams} type="radio" name="align" value="center"></input>
                    <label htmlFor="param-align-center">Середина</label>
                </div>
                <div className="value">
                    <input id="param-align-right" onChange={this.ChangeParams}  type="radio" name="align" value="right"></input>
                    <label htmlFor="param-align-right">Справа</label>
                </div>
            </div>
            <div className="form-group">
                <div className="label">Цвет текста:</div>
                <div className="value">
                    <input value={this.props.parameters.color} name="color" onChange={this.ChangeParams} type="color"></input>
                </div>
            </div>
        </div>;
    }
}

export default TextParameters;