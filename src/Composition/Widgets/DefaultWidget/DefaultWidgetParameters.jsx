import React from 'react';
import './WidgetParameters.css';

class DefaultWidgetParameters extends React.Component {
    constructor(props){
        super(props);
        this.state = props.parameters;
    }

    ChangeParams = (e) => {
        if(e.target.type === "checkbox")
            this.props.onParamChange(e.target.name, e.target.checked);
        else 
            this.props.onParamChange(e.target.name, e.target.value);
        
        var params = this.state;
        params[e.target.name] = e.target.value;
        this.setState(params);
    }
    
    render() {
        return <div className="defaultWidgetProps">
            <div className="group-label">Параметры виджета</div>
            <div className="form-group">
                <div className="value">
                    <input type="checkbox" onChange={this.ChangeParams} checked={this.props.parameters.hasBackGround} name="hasBackGround" id="param-hasBackGround"></input>
                    <label htmlFor="param-hasBackGround">Имеет задний фон</label>
                </div>
            </div>
            <div className="form-group">
                <div className="label">Цвет фона:</div>
                <div className="value">
                    <input value={this.props.parameters.backGroundColor} name="backGroundColor" onChange={this.ChangeParams} type="color"></input>
                </div>
            </div>
            <div className="form-group">
                <div className="label">Ширина границы:</div>
                <div className="value">
                    <input value={this.props.parameters.borderWidth} name="borderWidth" onChange={this.ChangeParams} type="number"></input>
                </div>
            </div>
            <div className="form-group">
                <div className="label">Цвет границы:</div>
                <div className="value">
                    <input value={this.props.parameters.borderColor} name="borderColor" onChange={this.ChangeParams} type="color"></input>
                </div>
            </div>
        </div>;
    }
}

export default DefaultWidgetParameters;