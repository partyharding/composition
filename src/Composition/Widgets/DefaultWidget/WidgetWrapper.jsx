import React from 'react';
import {Rnd} from 'react-rnd';

class WidgetWrapper extends React.Component {

    render(){
        return(
            <Rnd
                className={`widget-wrapper ${this.props.parameters.active && "active"}`}
                resizeGrid={[this.props.parameters.cellSize, this.props.parameters.cellSize]} 
                dragGrid={[this.props.parameters.cellSize * this.props.parameters.paperScale, this.props.parameters.cellSize * this.props.parameters.paperScale]} 
                bounds="parent"
                minWidth="50px"
                minHeight="50px"
                dragHandleClassName={this.props.dragHandler ? "dragHandler" : null}
                default={{
                    x:this.props.parameters.coord.x,
                    y:this.props.parameters.coord.y,
                    width: this.props.width,
                    height: this.props.height
                }}
                style={{
                    "backgroundColor": this.props.defaultparameters.hasBackGround ? this.props.defaultparameters.backGroundColor : "transparent",
                    "border": this.props.defaultparameters.borderWidth+"px solid "+this.props.defaultparameters.borderColor
                }}
                {...this.props.rndProps}
            >
                <div className="widget-actions">
                    <div className="remove widget-action" onClick={this.props.onWidgetRemove}></div>
                    {/* <div className="params widget-action" onClick={this.props.onChooseWidget}></div> */}
                    {
                        this.props.dragHandler &&
                        <div className="dragHandler widget-action"></div>
                    }
                </div>
                <div onClick={this.props.onChooseWidget} className="widget-content">
                    { this.props.children }
                </div>
            </Rnd>
        )
    }
}

export default WidgetWrapper;
