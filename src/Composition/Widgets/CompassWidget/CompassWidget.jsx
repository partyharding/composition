import React from 'react';
import DefaultWidgetParameters from '../DefaultWidget/DefaultWidgetParameters';
import WidgetWrapper from '../DefaultWidget/WidgetWrapper';
import CompassParameters from './CompassParameters';
import './CompassWidget.css';
import compassSvg from '../Icons/compass.svg';

class CompassWidget extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            "parameters":{
                "angle":0
            },
            "defaultParameters":{
                "hasBackGround":true,
                "backGroundColor":"#ffffff",
                "borderWidth":1,
                "borderColor":"#2191FB"
            }
        }
    }

    RenderParameters = () => {
        return  <div key={this.props.index}>
            <CompassParameters 
                parameters={this.state.parameters} 
                onParamChange={this.ChangeParameter}
            />
            <DefaultWidgetParameters
                parameters={this.state.defaultParameters} 
                onParamChange={this.ChangeDefaultParameter}
            />
        </div>
    }

    ChangeParameter = (paramName, newVal) => {
        //clone parameters to avoid direct state mutate warning
        var params = this.state.parameters;
        params[paramName] = newVal;
        this.setState({
            "parameters":params
        });
    }

    ChangeDefaultParameter = (paramName, newVal) => {
        var params = this.state.defaultParameters;
        params[paramName] = newVal;
        this.setState({
            "defaultParameters":params
        });
    }

    ChoseWidget = () => {
        this.props.onChooseWidget(this.props.index);
    }

    WidgetRemove = () => {
        this.props.onWidgetRemove(this.props.index);
    }

    render() {
        return (
            <WidgetWrapper
                width = {100}
                height = {100}
                parameters = {this.props}
                defaultparameters = {this.state.defaultParameters}
                onChooseWidget = {this.ChoseWidget}
                onWidgetRemove = {this.WidgetRemove}
                rndProps={{
                    scale:this.props.paperScale
                }}
            >
                <img alt="" style={
                        {
                            "transform":"rotate("+this.state.parameters.angle+"deg)",
                            "width":"100%",
                        }
                    } src={compassSvg}></img>
            </WidgetWrapper>
        )
    }
}

export default CompassWidget;