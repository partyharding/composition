import React from 'react';
import CompassWidget from './CompassWidget';
import './../DefaultWidget/WidgetItem.css';
import compassIcon from './../Icons/compass-3-line.svg';

class CompassItem extends React.Component {
    OnDragEnd = (e) => {
        this.props.onAddWidget(e, <CompassWidget/>, "compass");
    }

    render() {
        return <div className="widget-item" draggable onDragEnd={this.OnDragEnd}>
            <div className="widget-item-icon">
                <img alt="" src={compassIcon}></img>
            </div> 
            <div className="widget-item-label">
                Компас
            </div>
        </div>;
    }
}

export default CompassItem;