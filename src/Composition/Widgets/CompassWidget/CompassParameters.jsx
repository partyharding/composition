import React from 'react';
import './CompassParameters.css';

class CompassParameters extends React.Component {
    constructor(props){
        super(props);
        this.state = props.parameters;
    }

    ChangeParams = (e) => {
        if(e.target.type === "checkbox")
            this.props.onParamChange(e.target.name, e.target.checked);
        else 
            this.props.onParamChange(e.target.name, e.target.value);
        
        var params = this.state;
        params[e.target.name] = e.target.value;
        this.setState(params);
    }
    
    render() {
        return <div id="compassWidgetProps">
            <div className="group-label">Параметры компаса</div>
            <div className="form-group">
                <div className="label">Угол поворота:</div>
                <div>{this.props.parameters.angle}</div>
                <div className="value">
                    <input value={this.props.parameters.angle} name="angle" onChange={this.ChangeParams} min="0" max="360" type="range"></input>
                </div>
            </div>
        </div>;
    }
}

export default CompassParameters;