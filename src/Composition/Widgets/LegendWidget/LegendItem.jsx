import React from 'react';
import LegendWidget from './LegendWidget';
import './../DefaultWidget/WidgetItem.css';
import legendIcon from './../Icons/list-check-2.svg';

class LegendItem extends React.Component {
    OnDragEnd = (e) => {
        this.props.onAddWidget(e, <LegendWidget/>, "legend");
    }

    render() {
        return <div className="widget-item" draggable onDragEnd={this.OnDragEnd}>
            <div className="widget-item-icon">
                <img alt="" src={legendIcon}></img>
            </div> 
            <div className="widget-item-label">
                Легенда
            </div>
        </div>;
    }
}

export default LegendItem;