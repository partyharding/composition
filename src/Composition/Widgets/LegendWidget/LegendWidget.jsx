import React from 'react';
import DefaultWidgetParameters from '../DefaultWidget/DefaultWidgetParameters';
import WidgetWrapper from '../DefaultWidget/WidgetWrapper';
import LegendParameters from './LegendParameters';
import './LegendWidget.css';

class LegendWidget extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            "parameters":{
                "mapIndex":props.mapRefList.length > 0 ? 0 : null,
                "rows":5,
                "cols":2,
                "show":"visible",
                "fontSize":12,
                "padding":2
            },
            "spriteData":null,
            "layers":[],
            "defaultParameters":{
                "hasBackGround":true,
                "backGroundColor":"#ffffff",
                "borderWidth":1,
                "borderColor":"#2191FB"
            }
        }
        this.mapRef = props.mapRefList.length > 0 ?props.mapRefList[0].current : null;
    }

    RenderParameters = () => {
        return  <div key={this.props.index}>
            <LegendParameters 
                parameters={this.state.parameters} 
                onParamChange={this.ChangeParameter}
                mapRefList={this.props.mapRefList}
                onSetMapRef={this.SetMapRef}
            />
            <DefaultWidgetParameters
                parameters={this.state.defaultParameters} 
                onParamChange={this.ChangeDefaultParameter}
            />
        </div>
    }

    ChangeParameter = (paramName, newVal) => {
        //clone parameters to avoid direct state mutate warning
        var params = this.state.parameters;
        params[paramName] = newVal;
        this.setState({
            "parameters":params
        });

        if(paramName === "show")
            this.UpdateLegend();
    }

    ChangeDefaultParameter = (paramName, newVal) => {
        var params = this.state.defaultParameters;
        params[paramName] = newVal;
        this.setState({
            "defaultParameters":params
        });
    }

    SetMapRef = (index) => {
        //remove my ref from old map widger
        if(this.mapRef !== null)
            this.mapRef.RemoveLegendRef(this.props.index);

        //add me to new one
        this.mapRef = this.props.mapRefList[index].current;
        this.mapRef.AddLegendRef(this);

        this.UpdateLegend();
    }

    UnsetMapRef = () => {
        this.setState({"layers": [], "spriteData":null});
    }

    UpdateLegend = () => {
        var layers = [];
        if(this.state.parameters.show === "all")
            layers = this.mapRef.map.style.stylesheet.layers;
        
        if(this.state.parameters.show === "visible")
            layers = this.GetRenderedFeaturesLayers();

        this.setState({"layers":layers , "spriteData":this.mapRef.spriteData});
    }

    GetRenderedFeaturesLayers = () => {
        var mapLayers = this.mapRef.map.queryRenderedFeatures();
        var visibleLayersNames = [];
        var visibleLayers = [];
        
        for (var feature of mapLayers) {
            if(!visibleLayersNames.includes(feature.layer.id)){
                visibleLayers.push(feature.layer);
                visibleLayersNames.push(feature.layer.id);
            }
        }
        return visibleLayers;
    }

    componentDidMount = () => {
        if(this.mapRef !== null){
            this.mapRef.AddLegendRef(this);
            this.UpdateLegend();
        }
    }

    BuildLegendItem = (index) => {
        var icon = <div className="legend-icon-wrapper"></div>
        if(this.state.layers[index].layout && this.state.layers[index].layout["icon-image"]){
            var iconData = this.state.spriteData.positions[this.state.layers[index].layout["icon-image"]]
            var scale = iconData.width > iconData.height ? 18/iconData.width : 18/iconData.height;
            var iconStyles = {
                "backgroundImage":"url("+this.state.spriteData.image+")",
                "backgroundPosition":-iconData.x+"px "+-iconData.y+"px",
                "transformOrigin":"left top",
                "width": iconData.width+"px",
                "height": iconData.height+"px",
                "transform":"scale("+scale+")"
            };
            icon = <div className="legend-icon-wrapper">
                <div className="legend-icon" style={iconStyles}></div>
            </div>
        }
        return icon;
    }

    BuildTableArray = () => {
        var rows = [];
        var cells = [];
        if(this.state.layers.length > 0){
            for(var i = 1; i <= this.state.parameters.cols * this.state.parameters.rows; i++ ){
                if(i < this.state.layers.length){
                    var icon = this.BuildLegendItem(i-1);
                    var td = <td key={i} style={{"padding":this.state.parameters.padding+"px"}}>
                        {icon} - {this.state.layers[i-1].id}
                    </td>
                    cells.push(td);
                }
                    
                if(i % this.state.parameters.cols === 0){
                    rows.push(<tr key={"tr"+i}>{cells}</tr>);
                    cells = [];
                }
            }
        }
        return (<table style={{"fontSize": this.state.parameters.fontSize+"px"}}><tbody>{rows}</tbody></table>);
    }

    render() {
        return (
            <WidgetWrapper
                width = {400}
                height = {150}
                parameters = {this.props}
                defaultparameters = {this.state.defaultParameters}
                onChooseWidget = {()=>{this.props.onChooseWidget(this.props.index);}}
                onWidgetRemove = {()=>{this.props.onWidgetRemove(this.props.index)}}
                rndProps={{
                    scale:this.props.paperScale
                }}
            >
            
            <div className="legend-table" >
            {
                this.state.layers.length > 0 ?
                this.BuildTableArray()
                :
                <small>Нет ссылки на карту, или на карте нет слоев.</small>
            }
            </div>
            </WidgetWrapper>
        )
    }
}

export default LegendWidget;