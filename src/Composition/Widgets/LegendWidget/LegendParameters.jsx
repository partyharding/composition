import React from 'react';
import './LegendParameters.css';

class LegendParameters extends React.Component {
    constructor(props){
        super(props);
        this.state = props.parameters;
    }

    ChangeParams = (e) => {
        if(e.target.type === "checkbox")
            this.props.onParamChange(e.target.name, e.target.checked);
        else 
            this.props.onParamChange(e.target.name, e.target.value);
        
        var params = this.state;
        params[e.target.name] = e.target.value;
        this.setState(params);
    }
    
    render() {
        return <div id="legendWidgetProps">
            <div className="group-label">Параметры легенды</div>
            <div className="form-group">
                <div className="label">Карта:</div>
                <div className="value">
                <select name="mapIndex" value={this.state.mapIndex} onChange={
                        (e)=>{
                            this.props.onSetMapRef(e.target.value);
                            this.ChangeParams(e);
                        }
                    }>
                    {
                        this.props.mapRefList.map( (map, index) => {
                            if(map !== null && map.current !== null)
                                return <option key={index} value={index}>{map.current.state.parameters.name}</option>
                        })
                    }
                </select>
                </div>
            </div>
            <div className="form-group">
                <div className="label">Показать пункты:</div>
                <div className="value">
                    <input checked={this.state.show === "all"} id="param-show-all" onChange={this.ChangeParams} type="radio" name="show" value="all"></input>
                    <label htmlFor="param-show-all">Все</label>
                </div>
                <div className="value">
                    <input checked={this.state.show === "visible"} id="param-show-visible" onChange={this.ChangeParams} type="radio" name="show" value="visible"></input>
                    <label htmlFor="param-show-visible">Только видимые</label>
                </div>
            </div>
            <div className="form-group">
                <div className="label">Количество строк:</div>
                <div className="value">
                    <input type="number" name="rows" value={this.state.rows} onChange={this.ChangeParams}></input>
                </div>
                <div className="label">Количество столбцов:</div>
                <div className="value">
                    <input type="number" name="cols" value={this.state.cols} onChange={this.ChangeParams}></input>
                </div>
            </div>
            <div className="form-group">
                <div className="label">Размер шрифта:</div>
                <div className="value">
                    <input type="number" name="fontSize" value={this.state.fontSize} onChange={this.ChangeParams}></input>
                </div>
            </div>
            <div className="form-group">
                <div className="label">Размер отступов:</div>
                <div className="value">
                    <input type="number" name="padding" value={this.state.padding} onChange={this.ChangeParams}></input>
                </div>
            </div>
        </div>;
    }
}

export default LegendParameters;