import React from 'react';
import PaperView from './PaperView/PaperView';
import Parameters from './Parameters/Parameters';
import {TextItem, MapItem, CompassItem, LegendItem} from './Widgets';
import MapWidget from './Widgets/MapWidget/MapWidget';
import {PaperPropertiesAction, PaperFitToScreenAction, ExportAction} from './Actions';
import './Composition.css';
import './Widgets/DefaultWidget/Widget.css';

class Composition extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            "widgets":[],
            "showParameters":false,
            "currentTab":"widgets",
            "paperFormat":"",
            "paperOrientation":""
        }
        this.chosenWidgetIndex = null;
        this.paramsComponent = null;
        this.widgetRefs = [];
        this.paperViewRef = React.createRef();
        this.mapRefList = [];
    }

    AddWidget = (evt, widgetComponent, widgetName) => {
        var ref = React.createRef();
        this.widgetRefs.push(ref);
        var rect = document.querySelector("#paperview").getBoundingClientRect();
        this.state.widgets.push(
            {
                "name":widgetName,
                "component":widgetComponent, 
                "coord":{
                    "x":evt.pageX - (rect.x * this.paperViewRef.current.state.parameters.paperScale), 
                    "y":evt.pageY - (rect.y * this.paperViewRef.current.state.parameters.paperScale)
                }
            });

        if(widgetName === "map")
            this.mapRefList.push(this.widgetRefs[this.widgetRefs.length-1]);

        this.setState(this.state);
    }

    ChoseWidget = (index) => {
        if(this.chosenWidgetIndex !== index){
            // this.setState({"showParameters":true});
            this.SetParamsPanel(this.widgetRefs[index].current.RenderParameters());
            this.chosenWidgetIndex = index;
        } else {
            this.SetCurrentTab("inspector");
        }
    }

    RemoveWidget = (index) => {
        if(index === this.chosenWidgetIndex)
            this.setState({"showParameters":false});

        this.state.widgets.splice(index,1, null);
        this.widgetRefs.splice(index,1, null);
        this.Refresh();
    }

    SetCurrentTab = (tabname) => {
        this.setState({"currentTab":tabname});
    }

    SetParamsPanel = (component) => {
        this.paramsComponent = component;
        this.setState({"showParameters":true, "currentTab":"inspector"});
    }
    
    Refresh = () => {
        this.forceUpdate();
    }

    SetPaperFormat = () => {
        this.setState(
            {
                "paperFormat":this.paperViewRef.current.state.parameters.format,
                "paperOrientation":this.paperViewRef.current.state.parameters.orientation
            }
        );
    }

    componentDidMount = () => {
        var rect = document.querySelector("#paperview").getBoundingClientRect();
        this.AddWidget({"pageX":rect.x + rect.width/4, "pageY":rect.y + rect.height/4},<MapWidget key="firstmap" />, "map");
        this.SetPaperFormat();
    }
    
    render() {
        return (
            <div id="composition">
                <div className="composition-panel">
                    <div className="composition-tabs">
                        <div 
                            className={`composition-tab ${this.state.currentTab === "widgets" && "active"}`} 
                            onClick={()=>{this.SetCurrentTab("widgets")}}>Компоненты</div>
                        <div 
                            className={`composition-tab ${this.state.currentTab === "inspector" && "active"}`}
                            onClick={()=>{this.SetCurrentTab("inspector")}}>Параметры</div>
                    </div>
                    <div className="composition-tabs-content">
                        {
                            this.state.currentTab === "widgets" &&
                            <div className="composition-items">
                                <TextItem onAddWidget={this.AddWidget}/>
                                <MapItem onAddWidget={this.AddWidget}/>
                                <CompassItem onAddWidget={this.AddWidget}/>
                                <LegendItem onAddWidget={this.AddWidget}/>
                            </div>
                        }
                        {
                            this.state.currentTab === "inspector" &&
                            <div className="composition-params">
                                {
                                    this.state.showParameters &&
                                        <Parameters>
                                            {this.paramsComponent}
                                        </Parameters>
                                }
                                {
                                    !this.state.showParameters &&
                                    <small>Выберите какой-нибудь компонент.</small>
                                }
                            </div>
                        }
                    </div>
                </div>
                <div className="composition-paper">
                    <div className="composition-actions">
                        <PaperPropertiesAction compositionRef={this} paperViewRef={this.paperViewRef}/>
                        <PaperFitToScreenAction paperViewRef={this.paperViewRef}></PaperFitToScreenAction>
                        <ExportAction mapRefList={this.mapRefList} paperViewRef={this.paperViewRef} compositionRef={this}></ExportAction>
                    </div>
                    <div className="composition-format">
                        Формат листа - {this.state.paperFormat}, {this.state.paperOrientation === "portrait" ? "Книжная" : "Альбомная"}
                    </div>
                    <div className="composition-paperview">
                        <PaperView ref={this.paperViewRef} onChooseWidget={this.ChoseWidget} onRefresh={this.Refresh} onFormatChange={this.SetPaperFormat}>
                            {
                                this.state.widgets.map((widget, index) => {
                                    if(widget !== null)
                                    return (
                                        <widget.component.type 
                                            ref={this.widgetRefs[index]} 
                                            cellSize={parseInt(this.paperViewRef.current.state.parameters.cellSize)}
                                            paperScale={this.paperViewRef.current.state.parameters.paperScale}
                                            index={index}
                                            active={this.chosenWidgetIndex === index}
                                            onChooseWidget={this.ChoseWidget}
                                            onWidgetRemove={this.RemoveWidget}
                                            coord={widget.coord} 
                                            key={index}
                                            mapRefList = { widget.name === "legend" ? this.mapRefList : [] }
                                        />
                                    )
                                    else
                                    return null;
                                })
                            }
                        </PaperView>
                    </div>
                </div>
            </div>
        );
    }
}

export default Composition;