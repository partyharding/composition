import React from 'react';

class Parameters extends React.Component {
    render() {
        return this.props.children;
    }
}

export default Parameters;