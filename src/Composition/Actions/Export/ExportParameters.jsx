import React from 'react';
import '../../Widgets/DefaultWidget/WidgetParameters.css';
import { exportComponentAsPDF, exportComponentAsPNG } from 'react-component-export-image';
// import html2canvas from 'html2canvas';
// import canvasToImage from "canvas-to-image";

class ExportParameters extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            format:"PNG",
            quality:1,
            fileName:"Карта 01"
        };
        this.pRef = this.props.paperViewRef.current;
    }

    ExportFile = () => {
        // var res = [
        //     this.pRef.formatResolutions[this.pRef.state.parameters.format][1], 
        //     this.pRef.formatResolutions[this.pRef.state.parameters.format][0]
        // ];
        // var orientation = "portrait";
        // html2canvas(el,
        //     {
                
        //     }
        // ).then(canvas => {
        //     // const imgData = canvas.toDataURL('image/png');
        //     // var image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");  // here is the most important part because if you dont replace you will get a DOM 18 exception.
        //     // window.location.href=image;
        //     // console.log(imgData);
        //     // var a = document.createElement('a');
        //     // a.href = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
        //     // a.download = 'somefilename.png';
        //     // a.click();
        //     // this.tempDiv.append(canvas);
        // });

        if(this.state.format === "PNG"){
            exportComponentAsPNG(this.props.paperViewRef,{
                fileName:this.state.fileName,
                html2CanvasOptions:{
                    quality:this.state.quality
                }
            });
        }
        
        if(this.state.format === "PDF"){
            exportComponentAsPDF(this.props.paperViewRef,{
                fileName:this.state.fileName,
                html2CanvasOptions:{
                    quality:this.state.quality
                },
                pdfOptions:{
                }
            });
        }
    }

    ChangeParams = (e) => {
        var params = this.state;
        params[e.target.name] = e.target.value;
        this.setState(params);
    }

    render() {
        return (
            <div id="exportParameters">
                <div className="form-group">
                    <div className="label">Формат:</div>
                    <div className="value">
                        <input checked={this.state.format === "PNG"} id="export-format-png" onChange={this.ChangeParams} type="radio" name="format" value="PNG"></input>
                        <label htmlFor="export-format-png">PNG</label>
                    </div>
                    {/* <div className="value">
                        <input checked={this.state.format === "PDF"} id="export-format-pdf" onChange={this.ChangeParams} type="radio" name="format" value="PDF"></input>
                        <label htmlFor="export-format-pdf">PDF</label>
                    </div> */}
                </div>
                <div className="form-group">
                    <div className="label">Качество:</div>
                    <div className="value">
                        <input value={this.state.quality} name="quality" onChange={this.ChangeParams} step="0.1" min="0.1" max="1" type="range"></input>
                    </div>
                </div>
                <div className="form-group">
                    <div className="label">Имя файла:</div>
                    <div className="value">
                        <input value={this.state.fileName} name="fileName" onChange={this.ChangeParams} type="text"></input>
                    </div>
                </div>
                <button className="export-button" onClick={this.ExportFile}>Экспорт</button>
            </div>
        ) 
    }
}

export default ExportParameters;