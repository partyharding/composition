import React from 'react';
import './../Action.css';
import ExportParameters from './ExportParameters';
import icon from '../../Widgets/Icons/download-2-line.svg'

class ExportAction extends React.Component {
    constructor(props){
        super(props);
    }

    ShowProperies = () => {
        this.props.compositionRef.chosenWidgetIndex = null;
        this.props.compositionRef.SetParamsPanel(<ExportParameters mapRefList={this.props.mapRefList} paperViewRef={this.props.paperViewRef}/>);
    }

    render() {
        return <div className="action-item" onClick={this.ShowProperies}>
            <div className="action-item-icon">
                <img alt="" src={icon}></img>
            </div> 
            <div className="action-item-label">
                Экспорт
            </div>
        </div>;
    }
}

export default ExportAction;