import React from 'react';
import './../Action.css';
import paperIcon from '../../Widgets/Icons/file-paper-line.svg'

class PaperPropertiesAction extends React.Component {

    ShowProperies = () => {
        this.props.compositionRef.chosenWidgetIndex = null;
        this.props.compositionRef.SetParamsPanel(this.props.paperViewRef.current.RenderProperties());
    }

    render() {
        return <div className="action-item" onClick={this.ShowProperies}>
            <div className="action-item-icon">
                <img alt="" src={paperIcon}></img>
            </div> 
            <div className="action-item-label">
                Параметры листа
            </div>
        </div>;
    }
}

export default PaperPropertiesAction;