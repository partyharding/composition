import React from 'react';
import './../Action.css';
// import paperIcon from '../../Widgets/Icons/file-paper-line.svg'

class PaperFitToScreenAction extends React.Component {

    FitToScreen = () => {
        this.props.paperViewRef.current.FitToScreen();
    }

    render() {
        return <div className="action-item" onClick={this.FitToScreen}>
            
            <div className="action-item-label">
                По размеру экрана
            </div>
        </div>;
    }
}

export default PaperFitToScreenAction;