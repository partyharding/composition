import React from 'react';
import PaperProperties from './PaperProperties';
class PaperView extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            "parameters":{
                "backgroundColor":"#FFFFFF",
                "gridColor":"#EBEBEB",
                "cellSize":10,
                "format":"A4",
                "orientation":"landscape",
                "paperScale":1
            },
            "keyPressed":false
        }
        this.formatResolutions = {
            "A5":[437, 620],
            "A4":[620, 877],
            "A3":[877, 1240],
            "A2":[1240,1754],
            "A1":[1754,2483],
            "A0":[2483,3511]
        }
        this.scaleKeyCode = 17;
        this.isDragging = false;
        this.divRef = React.createRef();
        this.preventDefault = e => e.preventDefault();
        this.rect = null;
        this.scaleStep = 0.025;
    }

    ChangeParameter = (paramName, newVal) => {
        //clone parameters to avoid direct state mutate warning
        var params = this.state.parameters;
        params[paramName] = newVal;
        this.setState({
            "parameters":params
        },this.props.onRefresh());

        // if(paramName === "cellSize")
        //     this.props.onRefresh();

        if(paramName === "format" || paramName === "orientation")
            this.props.onFormatChange();
    }

    RenderProperties = () => {
        return(
            <PaperProperties parameters={this.state.parameters} onParamChange={this.ChangeParameter}/>
        )
    }

    componentDidMount () {
        this.divRef.current.addEventListener('wheel', this.preventDefault);
        document.addEventListener('keydown', this.KeyDown);
        document.addEventListener('keyup', this.KeyUp);
        this.rect = document.querySelector("#paperview").getBoundingClientRect();
    }
    
    componentWillUnmount () {
        this.divRef.current.removeEventListener('wheel', this.preventDefault);
        document.removeEventListener('keydown', this.KeyDown);
    }

    KeyDown = (e) => {
        if(e.keyCode === this.scaleKeyCode)
            // this.keyPressed = true;
            this.setState({"keyPressed":true});
    }

    KeyUp = (e) => {
        if(e.keyCode === this.scaleKeyCode){
            this.setState({"keyPressed":false});
            this.isDragging = false;
        }
    }

    OnWheel = (evt) => {
        var scale;
        if(this.state.keyPressed){
            if (evt.nativeEvent.wheelDelta < 0) {
                scale = this.state.parameters.paperScale - this.scaleStep;
            } else {
                scale = this.state.parameters.paperScale + this.scaleStep;
            }
            this.ScaleTo(scale);
        }
    }

    OnMouseDown = (evt) => {
        this.isDragging = true;
    }

    OnMouseUp = (evt) => {
        this.isDragging = false;
    }

    OnMouseMove = (evt) => {
        if(this.isDragging){
            this.divRef.current.scrollLeft -= evt.movementX;
            this.divRef.current.scrollTop -= evt.movementY;
        }
    }

    FitToScreen = () => {
        var scale;
        var height = this.divRef.current.getBoundingClientRect().height;

        if(this.state.parameters.orientation === "portrait")
            scale = height / this.formatResolutions[this.state.parameters.format][1];
        else
            scale = height / this.formatResolutions[this.state.parameters.format][0];
        
        this.ScaleTo(scale);
    }

    ScaleTo = (scale) => {
        var parameters = this.state.parameters;
        parameters.paperScale = scale;
        this.setState({"parameters":parameters}, this.props.onRefresh);
    }

    render() {
        return (
            <div
                className="paperview-wrapper"
                ref={this.divRef}
            >
                <div 
                    id="paperview"
                    style={
                            {
                                "backgroundColor":this.state.parameters.backgroundColor, 
                                "width": this.state.parameters.orientation === "portrait" ? 
                                    this.formatResolutions[this.state.parameters.format][0] : 
                                    this.formatResolutions[this.state.parameters.format][1], 
                                "height":this.state.parameters.orientation === "portrait" ? 
                                    this.formatResolutions[this.state.parameters.format][1] : 
                                    this.formatResolutions[this.state.parameters.format][0], 
                                "backgroundImage": "linear-gradient("+this.state.parameters.gridColor+" 1px, transparent 1px), linear-gradient(90deg, "+this.state.parameters.gridColor+" 1px, transparent 1px)",
                                "backgroundSize": this.state.parameters.cellSize+"px "+this.state.parameters.cellSize+"px",
                                "transform":"scale("+this.state.parameters.paperScale+")",
                            }
                        }>
                {
                    this.props.children
                }
                {
                    this.state.keyPressed &&
                    <div
                        className="paperview-drag-area"
                        onMouseDown={this.OnMouseDown}
                        onMouseUp={this.OnMouseUp}
                        onMouseMove={this.OnMouseMove}
                        onMouseLeave={this.OnMouseUp}
                        onWheel={this.OnWheel}
                    ></div>
                }
                </div>
            </div>
        )
    }
}

export default PaperView;