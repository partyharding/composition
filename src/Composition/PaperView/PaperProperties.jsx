import React from 'react';
import '../Widgets/DefaultWidget/WidgetParameters.css';
import './PaperView.css';

class PaperProperties extends React.Component {
    constructor(props){
        super(props);
        this.state = props.parameters;
    }

    ChangeParams = (e) => {
        this.props.onParamChange(e.target.name, e.target.value);

        var params = this.state;
        params[e.target.name] = e.target.value;
        this.setState(params);
    }

    render() {
        return (
            <div id="paperProps">
                <div className="form-group">
                    <div className="label">Формат:</div>
                    <div className="value">
                        <select name="format" value={this.state.format} onChange={this.ChangeParams}>
                            <option value="A5">А5</option>
                            <option value="A4">А4</option>
                            <option value="A3">А3</option>
                            <option value="A2">А2</option>
                            <option value="A1">А1</option>
                            <option value="A0">А0</option>
                        </select>
                    </div>
                </div>
                <div className="form-group">
                    <div className="label">Ориентация:</div>
                    <div className="value">
                        <input checked={this.state.orientation === "portrait"} id="param-orientation-portrait" onChange={this.ChangeParams} type="radio" name="orientation" value="portrait"></input>
                        <label htmlFor="param-orientation-portrait">Книжная</label>
                    </div>
                    <div className="value">
                        <input checked={this.state.orientation === "landscape"} id="param-orientation-landscape" onChange={this.ChangeParams} type="radio" name="orientation" value="landscape"></input>
                        <label htmlFor="param-orientation-landscape">Альбомная</label>
                    </div>
                </div>
                <div className="form-group">
                    <div className="label">
                        Размер клетки:
                    </div>
                    <div className="value">
                        <input name="cellSize" type="number" value={this.state.cellSize } onChange={this.ChangeParams}></input>
                    </div>
                </div>
                <div className="form-group">
                    <div className="label">Цвет фона:</div>
                    <div className="value">
                        <input name="backgroundColor" type="color" value={this.state.backgroundColor} onChange={this.ChangeParams}></input>
                    </div>
                </div>
            </div>
        ) 
    }
}

export default PaperProperties;